***Technology used: Java 11, Angular 10 (Recent angular version), Ionic 4, Cordova, Spring Boot 5, Mysql, Kotlin, Android***

This is a sample admin portal for the app
http://silarocks.rodionsolutions.com/

*Mobile app*
apk file can be found in the following link
https://drive.google.com/file/d/19PwQJs8gMWm74dpgGjUwHVU0x38URU3d/view?usp=sharing

The following app was originally developed for delivering construction materials but can be modified for any domain for example food, ecommerce, construction, logistics. Some of the features of the app which are different from other e commerce platforms are:

1. Multiple categories of price selection for example:
1kg costs $10 and 1 Piece costs $100 
![](https://rodionsolutions.com/gitlab/silarocks/1.png)

2. Dynamic Property and specification management
![](https://rodionsolutions.com/gitlab/silarocks/2.png)
![](https://rodionsolutions.com/gitlab/silarocks/3.png)


3. Dynamic Stock Management (If Maximum Stock is reached the user can not order more of the product)
![](https://rodionsolutions.com/gitlab/silarocks/4.png)

4. Consignment tracking with times entry such as:

order placed, out for delivery, reached destination and delivered times
![](https://rodionsolutions.com/gitlab/silarocks/5.png)
![](https://rodionsolutions.com/gitlab/silarocks/6.png)

![](https://rodionsolutions.com/gitlab/silarocks/7.png)
![](https://rodionsolutions.com/gitlab/silarocks/8.png)

5. Delivery time calculation by taking the longitude and latitude of source and destination with GPS guided shop recommendation according to distance calculation
![](https://rodionsolutions.com/gitlab/silarocks/9.png)

6. Brand category and product Management for multiple tenants

![](https://rodionsolutions.com/gitlab/silarocks/10.png)  
![](https://rodionsolutions.com/gitlab/silarocks/11.png)
![](https://rodionsolutions.com/gitlab/silarocks/12.png)  
![](https://rodionsolutions.com/gitlab/silarocks/13.png)
 

7. Order Management
![](https://rodionsolutions.com/gitlab/silarocks/14.png)

Below are some screenshots of the mobile app:

![](https://rodionsolutions.com/gitlab/silarocks/15.png) 
![](https://rodionsolutions.com/gitlab/silarocks/16.png)
![](https://rodionsolutions.com/gitlab/silarocks/17.png)
![](https://rodionsolutions.com/gitlab/silarocks/18.png)
![](https://rodionsolutions.com/gitlab/silarocks/19.png)
![](https://rodionsolutions.com/gitlab/silarocks/20.png)
![](https://rodionsolutions.com/gitlab/silarocks/21.png)
![](https://rodionsolutions.com/gitlab/silarocks/22.png)
![](https://rodionsolutions.com/gitlab/silarocks/23.png)
![](https://rodionsolutions.com/gitlab/silarocks/24.png)
![](https://rodionsolutions.com/gitlab/silarocks/25.png)
